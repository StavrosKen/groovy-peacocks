export const API_BASE = '/api';
export const SONGS = [
    {name: 'Funk', src: '/assets/songs/Funk-Groovy_Peacocks.mp3'},
    {name: 'Disorders', src: '/assets/songs/Disorders-Groovy_Peacocks.mp3'}
];
export const EVENTS = [
    {id: 'example-event', title: 'An example event!', content: 'This is a aample content of a event!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018', featured: true}, 
    {id: 'example-event', title: 'An example event!', content: 'This is a aample content of a event!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'},
    {id: 'example-event', title: 'An example event!', content: 'This is a aample content of a event!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'},
    {id: 'example-event', title: 'An example event!', content: 'This is a aample content of a event!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'},
    {id: 'example-event', title: 'An example event!', content: 'This is a aample content of a event!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'}
];
export const NEWS = [
    {id: 'example-new', title: 'An example new!', content: 'This is a aample content of a new!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018', featured: true}, 
    {id: 'example-new', title: 'An example new!', content: 'This is a aample content of a new!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'},
    {id: 'example-new', title: 'An example new!', content: 'This is a aample content of a new!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'},
    {id: 'example-new', title: 'An example new!', content: 'This is a aample content of a new!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'},
    {id: 'example-new', title: 'An example new!', content: 'This is a aample content of a new!', image: '/assets/images/news/easter-eggs.jpg', date: '29/04/2018'}
];
export const BAND = {image: '/assets/images/news/easter-eggs.jpg', content: 'This is the description of the band'};
export const MEDIA = {
    images:[
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'},
        {src: '/assets/images/gallery/image3.jpg'},
        {src: '/assets/images/gallery/image2.jpg'},
        {src: '/assets/images/gallery/image1.jpg'},
        {src: '/assets/images/news/easter-eggs.jpg'}
    ], 
        videos:[
            {thumbnail: '/assets/images/news/easter-eggs.jpg', src: 'youtube'}
        ]
    };
export const MEMBERS = [
    {id: 'nikos-kunn', name: 'Νίκος Αθανασιάδης', image: '/assets/images/news/easter-eggs.jpg', 
    bio: `Born in 12 of December 1990, Nikos came to life and thus the world had to change
    accordingly! It didn't take long for him to notice he could bend everything as he'd like :P .
    As a youngster I was a joyful and always an enthusiastic personality with lots of interest
    but specially hanging out with friends and listening to music. Music came into my life
    from a very young age since both of my parents were musicholics! It took me many
    years though to realize that I didn't just loved listening to music but playing too! When I
    was 15 yo my grandpa bought me my first music instrument. A Casio keyboard which
    introduced me to playing and not just listening. Always a self-taught, and still with no
    internet access I started playing simple melodies and chord progressions. Two years
    later my first guitar, a Fender squire standard, pulls me further into the world of music 
    and keeps me engaged for many hours a day. It was then that I realized, playing music
    was not just a simple excitement, but something that will definitely last.
    Seeing that, my family helped me study sound engineering after high school. I 
    met people with the same interests there. Also at that time and now with a sound
    engineering point of view, I fell in love with drumming!
    For almost two years I was saving money in order to buy my first drum kit! Finally, in the
    January of 2009 I bought my first drum kit! A Gretsch Catalina maple 10,12,14,16,22!
    Still have it, still love it!
    Playing the drums was more difficult than playing the guitar for me. Again self-taught but
    with the help of internet this time, I started playing the drums, first by myself and then in
    bands. I enjoy playing Rock, Rock’nRoll, Blues, Country, Glam Rock, Metal… And that’s
    what we did. “Looking for Jack” and ”Last minute project” are two bands that helped me
    develop my music skills…
    Around 2013 I started playing with my cousin Vagelis in “Looking for Jack” and this
    strengthened our bond. Together and with another friend, the guitarist, Christopher, we
    continued on to the “Last minute project”. After some time and since I had to move to
    another city we started looking for other musicians. We heard many people, till we found
    Dimitri! Little did we know that we were just one breath away from forming “Groovy
    Peacocks”! The last touch was the addition of Michael and there we had it!
    
    Things are going rock-crazy since then! We have plenty of ideas for songs. We are
    constantly writing new songs. We don’t have enough time to properly record everything.
    We are focusing our energy in writing new songs and in performing live!
    I don’t know how things will go on from here… I just know that music is a passion for me
    and that I like sharing these feelings with friends and people. It helps me be a better
    person. It makes life bearable. It makes me feel that the universe is doing its job the
    right way…`,
    equipment: `Drum kits:
    Acoustic set: Gretsch Catalina maple
    Tom-toms: 10”, 12”,
    Floor toms: 14”, 16”,
    Snare drum: 14”
    Bass drum: 22”
    Heads: Remo Emperor
    Drum hardware: Sonor &amp; Gibraltar
    Bass pedals:
    Sonor force 400 bass drum pedal,
    Millenium PD-669 stage double bass pedal
    Cymbals:
    Paiste 3 Ride”, Hi-Hat 14”,
    Drumstore TEC 16” Crash
    Zildjian ZBT 18” Crash,
    Meinl HCS 18” China
    Electronic set: Behringer XD80USB
    
    Guitars:
    Electric guitar: Fender squire standard, rosewood fret board, Antique burst
    
    Acoustic guitar: Harrison
    Keyboard: Casio CTK-671
    Sound Card: Tascam US-1800
    Sequencers: Cubase 5, Toontrack Superior Drummer 2.0
    Monitors: Samson Resolv A6 Active studio monitors
    Headphones: AKG K271MkII
    In-Ears: Shure SE215
    Drum mics: Samson 8KIT, Shure SM57`,
    links: {facebook: 'https://www.facebook.com/Nikoskunn'}}
];
