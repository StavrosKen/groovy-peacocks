import Vue from 'vue';

import { MEDIA } from 'src/config/constants';
import template from './media.html';

const animation = 'flipInX';
const animationDelay = 25; // in ms

export default Vue.component('media', {
  template,

  data() {
    return {
      mediaFilter: 'images',
      chunkedMedia: []
    };
  },
    created(){
        this.beltAnimations = true;
        this.media = MEDIA;
        this.fetchPosts(this.mediaFilter);
    },

  methods: {
    chunkArray(arr, n) {
        var rest = arr.length % n, // how much to divide
            restUsed = rest, // to keep track of the division over the elements
            partLength = Math.floor(arr.length / n),
            result = [];
    
        for(var i = 0; i < arr.length; i += partLength) {
            var end = partLength + i,
                add = false;
    
            if(rest !== 0 && restUsed) { // should add one element for the division
                end++;
                restUsed--; // we've used one division element now
                add = true;
            }
    
            result.push(arr.slice(i, end)); // part of the array
    
            if(add) {
                i++; // also increment i in the case we added an extra element for division
            }
        }
    
        return result;
    },
    fetchPosts(type){
        this.chunkedMedia = this.chunkArray(this.media[type], 3);
    },

    // Methods for transitions
    handleBeforeEnter(el) {
      el.style.opacity = 0;
      el.classList.add('animated');
    },

    handleEnter(el) {
      const delay = el.dataset.index * animationDelay;
      setTimeout(() => {
        el.style.opacity = 1;
        el.classList.add(animation);
      }, delay);
    }
  }
});
