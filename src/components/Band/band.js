import Vue from 'vue';

import { BAND, MEMBERS } from 'src/config/constants';
import template from './band.html';

export default Vue.component('band', {
  template,

  data() {
    return {
        band: BAND,
        members: MEMBERS
    };
  }
});
