import Vue from 'vue'; 
 
import { MEMBERS } from 'src/config/constants';
import template from './member.html'; 
 
export default Vue.component('member', {
  props: ['memberId'],
  template, 
 
  data() { 
    return { 
      post: {} 
    }; 
  }, 
 
  created(){ 
    this.fetchPost(); 
  }, 
 
  methods: { 
    fetchPost(){ 
      const id = this.$route.params.id || this.memberId; 
      this.member = MEMBERS.find(x => x.id === id );
    } 
  } 
}); 