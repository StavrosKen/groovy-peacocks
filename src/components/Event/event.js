import Vue from 'vue'; 
 
import { EVENTS } from 'src/config/constants';
import template from './event.html'; 
 
export default Vue.component('event', {
  props: ['eventId'],
  template, 
 
  data() { 
    return { 
      post: {} 
    }; 
  }, 
 
  created(){ 
    this.fetchPost(); 
  }, 
 
  methods: { 
    fetchPost(){ 
      const id = this.$route.params.id || this.eventId; 
      this.post = EVENTS.find(x => x.id === id );
    } 
  } 
}); 