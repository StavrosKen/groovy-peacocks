import Vue from 'vue';
import template from './player.html';
import { SONGS } from 'src/config/constants';

export default Vue.component('gp-player', {
  template,
  data:function(){
    return {
        currentSong: null,
        currentTime: '00:00',
        totalDisplayTime: '00:00',
        currentSeekTime: '0',
        totalTime: '0'
    }
  },
  created() {
    this.songs = SONGS;
    this.currentSongIndex = 0;
    this.currentSong = this.songs[this.currentSongIndex];
  },
  mounted() {
      var self = this;
      this.player = this.$refs['gp-player'];
      this.player.addEventListener("timeupdate", function() {
        self.currentTime = self.formatTime(self.player.currentTime);
      });
      this.player.addEventListener("loadeddata", function() {
        self.totalDisplayTime = self.formatTime(self.player.duration);
        self.totalTime = self.player.duration;
        self.currentTime = '00:00';
      });
  },
  methods: {
    changeSong(songIndex){
        this.currentSong = this.songs[songIndex];
        this.player.load();
        this.currentTime = 0;
        this.currentSeekTime = 0;
        this.player.play();
    },
    previousSong: function() {
        this.currentSongIndex = this.currentSongIndex === 0 ? this.songs.length - 1 : this.currentSongIndex - 1;
        this.changeSong(this.currentSongIndex);
    },
    nextSong: function() {
        this.currentSongIndex = this.currentSongIndex === this.songs.length - 1 ? 0 : this.currentSongIndex + 1;
        this.changeSong(this.currentSongIndex);
    },
    playToggle: function() {
        if (this.player.paused){
            this.player.play();
        }
        else{
            this.player.pause();
        }
    },
    volumeChange: function(e) {
        this.player.volume = e.target.value;
    },
    seekChange: function(e) {
        var percent = e.offsetX / e.target.clientWidth;
        this.player.currentTime = percent * this.player.duration;
        this.currentSeekTime = percent * this.player.duration;
    },
    songEnded: function() {
        this.nextSong();
    },
    formatTime: function(time) {
        var minutes = Math.round(time/60);
        var seconds = Math.round(time%60);
        if (seconds < 10){
            seconds = 0 + '' + seconds;
        }
        return minutes + ':' + seconds;
     }
  },
});

//deny direct access to mp3 files