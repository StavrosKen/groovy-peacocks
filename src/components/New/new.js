import Vue from 'vue'; 
 
import { NEWS } from 'src/config/constants';
import template from './new.html'; 
 
export default Vue.component('new', {
  props: ['newId'],
  template, 
 
  data() { 
    return { 
      post: {} 
    }; 
  }, 
 
  created(){ 
    this.fetchPost(); 
  }, 
 
  methods: { 
    fetchPost(){ 
      const id = this.$route.params.id || this.newId; 
      this.post = NEWS.find(x => x.id === id );
    } 
  } 
}); 