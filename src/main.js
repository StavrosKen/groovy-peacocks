import Vue from 'vue';
import VueRouter from 'vue-router';
import { LoadingState } from 'src/config/loading-state';
import Navigation from 'components/Navigation/navigation';
import Footer from 'components/Footer/footer';
import Loader from 'components/Loader/loader';
import Player from 'components/Player/player';
import 'src/config/http';
import routes from 'src/routes';
import 'src/style.scss';

Vue.use(VueRouter);

export const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'active'
});

new Vue({
  router,
  components: {
    Navigation,
    'app-footer':Footer,
    Loader,
    'gp-player': Player
  },

  data(){
    return {
      isLoading: false
    };
  },

  created(){
    LoadingState.$on('toggle', (isLoading) => {
      this.isLoading = isLoading;
    });
  }
}).$mount('#app');
