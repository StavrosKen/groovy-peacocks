import Home from 'components/Home/home';
import News from 'components/News/news';
import New from 'components/New/new';
import Events from 'components/Events/events';
import Event from 'components/Event/Event';
import Member from 'components/Member/member';
import Band from 'components/Band/band';
import Media from 'components/Media/media';
import Contact from 'components/Contact/contact';
import NotFound from 'components/NotFound/notFound';

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/contact',
    component: Contact
  },
  {
    path: '/news',
    component: News
  },
  { 
    path: '/news/:id',
    name: 'new', 
    component: New 
  },
  {
    path: '/events',
    component: Events
  },
  { 
    path: '/events/:id',
    name: 'event', 
    component: Event 
  },
  {
    path: '/band',
    component: Band
  },
  { 
    path: '/member/:id',
    name: 'member', 
    component: Member 
  },
  {
    path: '/media',
    component: Media
  },
  {
    path: '*',
    component: NotFound
  }
];

export default routes;
